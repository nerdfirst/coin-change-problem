# denominations = [1, 2, 3, 5]
# changeAmount = 9

denominations = [1, 5, 10, 20, 50, 100]
changeAmount = 756


memo = [[0 for i in range(changeAmount+1)] for j in range(len(denominations)+1)]
for i in range(changeAmount + 1):
	memo[0][i] = i

for i in range(1, len(denominations)+1):
	for j in range(1, changeAmount+1):
		
		if denominations[i-1] == j:
			# If this coin can exactly change for the amount indicated by the
			# column, we pick only this coin
			memo[i][j] = 1
			
		elif denominations[i-1] > j:
			# If the current denomination is too small to be added to the current
			# column limit, we don't include it
			memo[i][j] = memo[i-1][j]
			
		else:
			largestStartingValue = j - denominations[i-1]
			switchOff = memo[i-1][j]
			switchOn = memo[i][largestStartingValue] + 1
			memo[i][j] = min((switchOff, switchOn))
			
for row in memo:
	print(row)