# denominations = [1, 20, 30, 50]
# value = 90

denominations = [1, 5, 10, 20, 50, 100]
value = 756

def makeChange(pos, coins):
	total = 0
	for i in range(len(coins)):
		total += coins[i] * denominations[i]
		
	# Base case: Correct amount
	if total == value:
		return sum(coins)
		
	# Base Case: Amount exceeded
	if total > value:
		return False
		
	# Base Case: No coins left
	if pos >= len(denominations):
		return False
		
	# Recursive Case: Explore possibilities
	current = denominations[pos]
	count = 0
	minCoins = 9999999999
	
	while total + (count*current) <= value:
		newCoins = coins.copy()
		newCoins[pos] = count
		ans = makeChange(pos+1, newCoins)
		if ans!=False and ans<minCoins:
			minCoins = ans
		
		count += 1
		
	return minCoins
		
ans = makeChange(0, [0,0,0,0,0,0])
print(ans)

